﻿#pragma once
#include "HuffMain.h"



using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

public ref class MyForm : public System::Windows::Forms::Form
{
public:
	MyForm(void)
	{
		InitializeComponent();
	}

protected:
	~MyForm()
	{
		if (components)
		{
			delete components;
		}
	}
private: System::Windows::Forms::TextBox^  dir;

protected:
private: System::Windows::Forms::Label^  label1;
private: System::Windows::Forms::ListView^  listView1;
private: System::Windows::Forms::Button^  bLoadFolder;
private: System::Windows::Forms::Button^  bCompress;
private: System::Windows::Forms::Button^  bLoadFile;
private: System::Windows::Forms::Button^  bDeCompress1;
private: System::Windows::Forms::Button^  bDeCompress2;





private: System::Windows::Forms::RadioButton^  radioButton1;
private: System::Windows::Forms::RadioButton^  radioButton2;
private: System::Windows::Forms::FolderBrowserDialog^  dgfolder;
private: System::Windows::Forms::OpenFileDialog^  dgfopen;
private: System::Windows::Forms::SaveFileDialog^  dgfsave;
private: System::Windows::Forms::ColumnHeader^  columnHeader1;
private: System::Windows::Forms::ColumnHeader^  columnHeader2;
private: System::Windows::Forms::ColumnHeader^  columnHeader3;

private:
	System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
	void InitializeComponent(void)
	{
		this->dir = (gcnew System::Windows::Forms::TextBox());
		this->label1 = (gcnew System::Windows::Forms::Label());
		this->listView1 = (gcnew System::Windows::Forms::ListView());
		this->columnHeader1 = (gcnew System::Windows::Forms::ColumnHeader());
		this->columnHeader2 = (gcnew System::Windows::Forms::ColumnHeader());
		this->columnHeader3 = (gcnew System::Windows::Forms::ColumnHeader());
		this->bLoadFolder = (gcnew System::Windows::Forms::Button());
		this->bCompress = (gcnew System::Windows::Forms::Button());
		this->bLoadFile = (gcnew System::Windows::Forms::Button());
		this->bDeCompress1 = (gcnew System::Windows::Forms::Button());
		this->bDeCompress2 = (gcnew System::Windows::Forms::Button());
		this->radioButton1 = (gcnew System::Windows::Forms::RadioButton());
		this->radioButton2 = (gcnew System::Windows::Forms::RadioButton());
		this->dgfolder = (gcnew System::Windows::Forms::FolderBrowserDialog());
		this->dgfopen = (gcnew System::Windows::Forms::OpenFileDialog());
		this->dgfsave = (gcnew System::Windows::Forms::SaveFileDialog());
		this->SuspendLayout();
		// 
		// dir
		// 
		this->dir->Location = System::Drawing::Point(12, 25);
		this->dir->Name = L"dir";
		this->dir->ReadOnly = true;
		this->dir->Size = System::Drawing::Size(415, 20);
		this->dir->TabIndex = 0;
		this->dir->Text = L"..................";
		// 
		// label1
		// 
		this->label1->AutoSize = true;
		this->label1->Location = System::Drawing::Point(9, 9);
		this->label1->Name = L"label1";
		this->label1->Size = System::Drawing::Size(197, 13);
		this->label1->TabIndex = 1;
		this->label1->Text = L"Đường dẫn thư mục hoặc file hiện hành:";
		// 
		// listView1
		// 
		this->listView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(3) {
			this->columnHeader1, this->columnHeader2,
				this->columnHeader3
		});
		this->listView1->Location = System::Drawing::Point(12, 51);
		this->listView1->Name = L"listView1";
		this->listView1->Size = System::Drawing::Size(415, 399);
		this->listView1->TabIndex = 2;
		this->listView1->UseCompatibleStateImageBehavior = false;
		this->listView1->View = System::Windows::Forms::View::Details;
		// 
		// columnHeader1
		// 
		this->columnHeader1->Text = L"File Name";
		this->columnHeader1->Width = 200;
		// 
		// columnHeader2
		// 
		this->columnHeader2->Text = L"File Size";
		this->columnHeader2->Width = 100;
		// 
		// columnHeader3
		// 
		this->columnHeader3->Text = L"Compressed Size";
		this->columnHeader3->Width = 100;
		// 
		// bLoadFolder
		// 
		this->bLoadFolder->Location = System::Drawing::Point(447, 51);
		this->bLoadFolder->Name = L"bLoadFolder";
		this->bLoadFolder->Size = System::Drawing::Size(125, 50);
		this->bLoadFolder->TabIndex = 3;
		this->bLoadFolder->Text = L"Duyệt Thư Mục";
		this->bLoadFolder->UseVisualStyleBackColor = true;
		this->bLoadFolder->Click += gcnew System::EventHandler(this, &MyForm::bLoadFolder_Click);
		// 
		// bCompress
		// 
		this->bCompress->Enabled = false;
		this->bCompress->Location = System::Drawing::Point(447, 107);
		this->bCompress->Name = L"bCompress";
		this->bCompress->Size = System::Drawing::Size(125, 50);
		this->bCompress->TabIndex = 4;
		this->bCompress->Text = L"Nén toàn bộ thư mục";
		this->bCompress->UseVisualStyleBackColor = true;
		this->bCompress->Click += gcnew System::EventHandler(this, &MyForm::bCompress_Click);
		// 
		// bLoadFile
		// 
		this->bLoadFile->Location = System::Drawing::Point(447, 218);
		this->bLoadFile->Name = L"bLoadFile";
		this->bLoadFile->Size = System::Drawing::Size(125, 50);
		this->bLoadFile->TabIndex = 5;
		this->bLoadFile->Text = L"Duyệt Tập tin nén";
		this->bLoadFile->UseVisualStyleBackColor = true;
		this->bLoadFile->Click += gcnew System::EventHandler(this, &MyForm::bLoadFile_Click);
		// 
		// bDeCompress1
		// 
		this->bDeCompress1->Enabled = false;
		this->bDeCompress1->Location = System::Drawing::Point(447, 274);
		this->bDeCompress1->Name = L"bDeCompress1";
		this->bDeCompress1->Size = System::Drawing::Size(125, 50);
		this->bDeCompress1->TabIndex = 6;
		this->bDeCompress1->Text = L"Giải nén các file đã chọn";
		this->bDeCompress1->UseVisualStyleBackColor = true;
		this->bDeCompress1->Click += gcnew System::EventHandler(this, &MyForm::bDeCompress1_Click);
		// 
		// bDeCompress2
		// 
		this->bDeCompress2->Enabled = false;
		this->bDeCompress2->Location = System::Drawing::Point(447, 330);
		this->bDeCompress2->Name = L"bDeCompress2";
		this->bDeCompress2->Size = System::Drawing::Size(125, 50);
		this->bDeCompress2->TabIndex = 7;
		this->bDeCompress2->Text = L"Giải nén tất cả";
		this->bDeCompress2->UseVisualStyleBackColor = true;
		this->bDeCompress2->Click += gcnew System::EventHandler(this, &MyForm::bDeCompress2_Click);
		// 
		// radioButton1
		// 
		this->radioButton1->AutoSize = true;
		this->radioButton1->Enabled = false;
		this->radioButton1->Location = System::Drawing::Point(447, 386);
		this->radioButton1->Name = L"radioButton1";
		this->radioButton1->Size = System::Drawing::Size(80, 17);
		this->radioButton1->TabIndex = 8;
		this->radioButton1->TabStop = true;
		this->radioButton1->Text = L"Chọn tất cả";
		this->radioButton1->UseVisualStyleBackColor = true;
		this->radioButton1->CheckedChanged += gcnew System::EventHandler(this, &MyForm::radioButton1_CheckedChanged);
		// 
		// radioButton2
		// 
		this->radioButton2->AutoSize = true;
		this->radioButton2->Enabled = false;
		this->radioButton2->Location = System::Drawing::Point(447, 409);
		this->radioButton2->Name = L"radioButton2";
		this->radioButton2->Size = System::Drawing::Size(95, 17);
		this->radioButton2->TabIndex = 9;
		this->radioButton2->TabStop = true;
		this->radioButton2->Text = L"Bỏ chọn tất cả";
		this->radioButton2->UseVisualStyleBackColor = true;
		this->radioButton2->CheckedChanged += gcnew System::EventHandler(this, &MyForm::radioButton2_CheckedChanged);
		// 
		// dgfopen
		// 
		this->dgfopen->FileName = L"openFileDialog1";
		this->dgfopen->Filter = L"Huffman File|*.huff|All Files|*.*";
		// 
		// dgfsave
		// 
		this->dgfsave->Filter = L"Huffman File|*.huff|All Files|*.*";
		// 
		// MyForm
		// 
		this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
		this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
		this->ClientSize = System::Drawing::Size(584, 462);
		this->Controls->Add(this->radioButton2);
		this->Controls->Add(this->radioButton1);
		this->Controls->Add(this->bDeCompress2);
		this->Controls->Add(this->bDeCompress1);
		this->Controls->Add(this->bLoadFile);
		this->Controls->Add(this->bCompress);
		this->Controls->Add(this->bLoadFolder);
		this->Controls->Add(this->listView1);
		this->Controls->Add(this->label1);
		this->Controls->Add(this->dir);
		this->MaximizeBox = false;
		this->Name = L"MyForm";
		this->Text = L"Chuong trinh nen file theo thuat toan Huffman";
		this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
		this->Closed += gcnew System::EventHandler(this, &MyForm::MyForm_Closed);
		this->ResumeLayout(false);
		this->PerformLayout();

	}
#pragma endregion

	public:
		NenFile *cs;
		GiaiNenFile *de;
		char *si, *so;

private: System::Void bLoadFolder_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void bCompress_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void bLoadFile_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void bDeCompress1_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void bDeCompress2_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void radioButton1_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
private: System::Void radioButton2_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e);
private: System::Void MyForm_Closed(System::Object^  sender, System::EventArgs^  e);

};