#pragma once
#define _CRT_SECURE_NO_WARNINGS
#define MAXNODE 511

#include <iostream>
#include <cstdint>
#include <string>
#include <Windows.h>

using namespace std;
typedef struct
{
	unsigned char c;
	long nFreq;
	int nLeft;
	int nRight;
} HUFFNode;
typedef struct
{
	char fname[127];	// ten file
	char NullBit;		// so luong bit du
	uint64_t fsize;		// kt cu cua file
	uint64_t ps;		// vi tri bat dau
	uint64_t pe;		// vi tri ket thuc	
}FileInfo;

class NenFile
{
public:
	uint64_t tanso[256];	// 8 * 256 bytes
	uint32_t sotaptin;		// 4 bytes
	FileInfo *danhsach;		// sotaptin * 152 bytes

	char mabit[256][20];
	HUFFNode HuffTree[MAXNODE];

	// phuong thuc
	void lammoi();
	void laythongtincacfile(char *thumuc);
	void docfile(char *taptin, int i);
	void taocayhuff();
	void taomabit(char *a, int i, int root);
	void xuatfiletam(char *thumuc, int i);
	void xuatfilechinh(char *thumuc, char *taptin);
	
};

// class giai nen

class GiaiNenFile
{
public:
	uint64_t tanso[256];
	uint32_t sotaptin;
	FileInfo *danhsach;
	FILE *f;
	HUFFNode HuffTree[MAXNODE];
	int nroot;

	void lammoi();
	void docfile(char *taptin);
	void taocayhuff();
	void xuatfile(char *thumuc, int i);
};