﻿#include "MyForm.h"

namespace gui {    // Change this!!
	using namespace System;
	using namespace System::Windows::Forms;
	using namespace System::Drawing;

	[STAThread]
	int main(array<System::String ^> ^args)
	{
		Application::EnableVisualStyles();
		Application::SetCompatibleTextRenderingDefault(false);
		Application::Run(gcnew MyForm());
		return 0;
	}
	Compression cs;
	DeCompression de;
	char si[260] = { 0 }, so[260];
	char mask[1000];
	Void MyForm::MyForm_Load(System::Object^  sender, System::EventArgs^  e)
	{
		listView1->View = View::Details;
		pictureBox1->Image = Image::FromFile("image.png");

	}
	Void MyForm::button1_Click(System::Object^  sender, System::EventArgs^  e) {
		System::Windows::Forms::DialogResult res = folder->ShowDialog();
		if (res == System::Windows::Forms::DialogResult::OK)
		{
			String ^t1 = folder->SelectedPath;
			textBox1->Text = t1;
			sprintf(si, "%s", t1);
			char st[260];
			sprintf(st, "%s\\*.*", t1);
			cs.GetFileName(st);
			for (int i; i < cs.filenum; i++)
			{
				t1 = gcnew String(cs.list[i].fname);
				listView1->Items->Add(t1);
			}
		}
	}
	Void MyForm::button2_Click(System::Object^  sender, System::EventArgs^  e)
	{
		if (cs.filenum == 0)
			return;
		System::Windows::Forms::DialogResult res = fileo->ShowDialog();
		if (res == System::Windows::Forms::DialogResult::OK)
		{
			char buff[30];
			String ^t1 = fileo->FileName;
			sprintf(so, "%s", t1);
			cs.Compress(si, so);
			listView1->Items->Clear();
			for (int i = 0; i < cs.filenum; i++)
			{
				ListViewItem t;
				array<String^>^ subItems = gcnew array<String^>(4);
				subItems[0] = gcnew String(cs.list[i].fname);
				sprintf(buff, "%ld", cs.list[i].fsize);
				subItems[1] = gcnew String(buff);
				sprintf(buff, "%ld", cs.list[i].pe - cs.list[i].ps);
				subItems[2] = gcnew String(buff);
				sprintf(buff, "%lf", (double)(cs.list[i].pe - cs.list[i].ps) / (double)(cs.list[i].fsize));
				subItems[3] = gcnew String(buff);
				ListViewItem^ itm = gcnew ListViewItem(subItems);
				// Add the ListViewItem to the ListView
				listView1->Items->Add(itm);
			}
			MessageBox::Show("Thành công", "Trạng thái");
			cs.Renew();
			memset(si, 0, 260);
			memset(so, 0, 260);
		}
	}
	Void MyForm::button3_Click(System::Object^  sender, System::EventArgs^  e)
	{
		if (si[0] != 0)
			return;
		System::Windows::Forms::DialogResult res = filei->ShowDialog();
		if (res == System::Windows::Forms::DialogResult::OK)
		{
			String ^t1 = filei->FileName;
			textBox1->Text = t1;
			sprintf(si, "%s", t1);
			de.Input(si);
			if (de.filenum == 0)
			{
				fclose(de.f);
				MessageBox::Show("File khong ho tro", "Trạng thái");
				return;
			}
			
			listView1->Items->Clear();
			char buff[30] = { 0 };
			for (int i = 0; i < de.filenum; i++)
			{
				ListViewItem t;
				array<String^>^ subItems = gcnew array<String^>(4);
				subItems[0] = gcnew String(de.list[i].fname);
				sprintf(buff, "%ld", de.list[i].fsize);
				subItems[1] = gcnew String(buff);
				sprintf(buff, "%ld", de.list[i].pe - de.list[i].ps);
				subItems[2] = gcnew String(buff);
				sprintf(buff, "%lf", (double)(de.list[i].pe - de.list[i].ps) / (double)(de.list[i].fsize));
				subItems[3] = gcnew String(buff);
				ListViewItem^ itm = gcnew ListViewItem(subItems);
				// Add the ListViewItem to the ListView
				listView1->Items->Add(itm);
			}

			memset(mask, 0, 1000);
		}
	}
	Void MyForm::button4_Click(System::Object^  sender, System::EventArgs^  e)
	{
		System::Windows::Forms::DialogResult res = folder->ShowDialog();
		if (res == System::Windows::Forms::DialogResult::OK)
		{
			de.MakeHuffTree();
			int a = 0;
			String ^t1 = folder->SelectedPath;
			sprintf(so, "%s", t1);
			for (int i = 0; i < listView1->CheckedItems->Count; i++)
			{
				int j = listView1->Items->IndexOf(listView1->CheckedItems[i]);
				de.Output(j, so);
			}
		}
		fclose(de.f);
	}
}

