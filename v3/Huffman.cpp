#define MAXBUFF 8

#include "Huffman.h"

Compression::Compression()
{
	int i = 256;
	while (--i >= 0)
	{
		HuffTree[i].c = i;
		memset(tBit[i], 0, 20);
	}
	for (i = 0; i < 512; i++)
	{

		HuffTree[i].nFreq = 0;
		HuffTree[i].nLeft = HuffTree[i].nRight = -1;
	}
	list = NULL;
	strcpy(bitsign, "nhom9ctdl&gt");
	filenum = 0;
}
Compression::~Compression()
{
	delete[]list;
}
void Compression::Renew()
{
	delete[]list;
	list = NULL;
	filenum = 0;
	int i = 256;
	while (--i >= 0)
	{
		HuffTree[i].c = i;
		memset(tBit[i], 0, 20);
	}
	for (i = 0; i < 512; i++)
	{

		HuffTree[i].nFreq = 0;
		HuffTree[i].nLeft = HuffTree[i].nRight = -1;
	}
}
void Compression::GetFileName(char *s)
{
	WIN32_FIND_DATA fileData;
	HANDLE hFind;
	// get num file
	if (!((hFind = FindFirstFile(s, &fileData)) == INVALID_HANDLE_VALUE))
	{
		do
		{
			if (!(fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				filenum++;
		} while (FindNextFile(hFind, &fileData));
	}
	FindClose(hFind);
	// list num file
	if (filenum == 0)
		return;
	list = new FileInfo[filenum];
	int i = 0;
	if (!((hFind = FindFirstFile(s, &fileData)) == INVALID_HANDLE_VALUE))
	{
		do
		{
			if (!(fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				strcpy(list[i].fname, fileData.cFileName);
				i++;
			}
		} while (FindNextFile(hFind, &fileData));
	}
	FindClose(hFind);
}
void Compression::Compress(char *si, char *so)
{
	char st[256], sto[256];
	int  i, j;
	// doc file -> lap bang tan so
	for (i = 0; i < filenum; i++)
	{
		sprintf(st, "%s\\%s", si, list[i].fname);
		Input(st, i);
	}
	MakeHuffTree();
	// xuat file phu
	FILE *f;
	for (i = 0; i < filenum; i++)
	{
		sprintf(st, "%s\\%s", si, list[i].fname);
		sprintf(sto, "%s\\%s.tmp", si, list[i].fname);
		Output(sto, st, i);
		f = fopen(sto, "rb");
		fseek(f, 0L, SEEK_END);
		list[i].pe = ftell(f);
		fclose(f);
		if (i == 0)
		{
			list[i].ps = 5136 + filenum * sizeof(FileInfo);
			list[i].pe += list[i].ps;
		}
		else
		{
			list[i].ps = list[i - 1].pe;
			list[i].pe += list[i].ps;
		}
	}
	// xuat file chinh
	FILE *fi;
	f = fopen(so, "wb");
	fwrite(bitsign, 1, 12, f);
	fwrite(tBit, 1, 5120, f);
	fwrite(&filenum, sizeof(uint32_t), 1, f);
	fwrite(list, sizeof(FileInfo), filenum, f);
	int n, nt;
	unsigned char buff[MAXBUFF];
	for (i = 0; i < filenum; i++)
	{
		sprintf(sto, "%s\\%s.tmp", si, list[i].fname);
		fi = fopen(sto, "rb");
		fseek(fi, 0L, SEEK_END);
		n = ftell(fi);
		rewind(fi);
		nt = n % MAXBUFF;
		n = n / MAXBUFF;
		for (int j = 0; j < n; j++)
		{
			fread(buff, 1, MAXBUFF, fi);
			fwrite(buff, 1, MAXBUFF, f);
		}
		fread(buff, 1, nt, fi);
		fwrite(buff, 1, nt, f);
		fclose(fi);
		// xoa file phu
		remove(sto);
	}
	fclose(f);
}
bool Compression::Input(char *s, int i)
{
	FILE *f;
	f = fopen(s, "rb");
	if (f == NULL)
		return false;

	long n, nt;
	unsigned char buff[MAXBUFF];
	// xac dinh kich thuoc tap tin
	fseek(f, 0L, SEEK_END);
	n = ftell(f);
	list[i].fsize = n;
	rewind(f);
	// doc file
	nt = n % MAXBUFF;
	n = n / MAXBUFF;
	for (int i = 0; i < n; i++)
	{
		fread(buff, 1, MAXBUFF, f);
		HuffTree[buff[0]].nFreq++;
		HuffTree[buff[1]].nFreq++;
		HuffTree[buff[2]].nFreq++;
		HuffTree[buff[3]].nFreq++;
		HuffTree[buff[4]].nFreq++;
		HuffTree[buff[5]].nFreq++;
		HuffTree[buff[6]].nFreq++;
		HuffTree[buff[7]].nFreq++;
	}
	fread(buff, 1, nt, f);
	while (--nt >= 0)
		HuffTree[buff[nt]].nFreq++;
	fclose(f);
	return true;
}
void Compression::MakeHuffTree()
{
	HUFFNode tmp1;
	int n = 0;
	for (int i = 0; i < 256; i++)
	if (HuffTree[i].nFreq >0)
	{
		if (i > n)
		{
			while (HuffTree[n].nFreq > 0) n++;
			tmp1 = HuffTree[i];
			HuffTree[i] = HuffTree[n];
			HuffTree[n] = tmp1;
			n++;
		}
		else if (i == n)
		{
			n++;
		}
	}
	int id = 0;
	char mask[512] = { 0 };
	while (id < n - 1)
	{
		int im1 = -1, im2 = -1;
		for (int i = 0; i < n; i++)
		{
			if (mask[i] == 1)
				continue;
			if (im2 == -1 || HuffTree[im2].nFreq > HuffTree[i].nFreq)
				im2 = i;
			else if (HuffTree[im2].nFreq == HuffTree[i].nFreq && HuffTree[im2].c > HuffTree[i].c)
				im2 = i;
		}
		mask[im2] = 1;
		for (int i = 0; i < n; i++)
		{
			if (mask[i] == 1)
				continue;
			if (im1 == -1 || HuffTree[im1].nFreq > HuffTree[i].nFreq)
				im1 = i;
			else if (HuffTree[im2].nFreq == HuffTree[i].nFreq && HuffTree[im2].c > HuffTree[i].c)
				im1 = i;
		}
		mask[im1] = 1;
		if (im1 == -1)
		{
			mask[511] = 1;
		}

		HuffTree[n].nFreq = HuffTree[im1].nFreq + HuffTree[im2].nFreq;
		HuffTree[n].c = HuffTree[im2].c;
		HuffTree[n].nLeft = im2;
		HuffTree[n].nRight = im1;
		n++;
		id += 2;
	}
	// n-1 : dinh cay
	char a[20] = { 0 };
	MakeBit(a, 0, n - 1);
}
void Compression::MakeBit(char *a, int i, int root)
{
	if (HuffTree[root].nLeft == -1 && HuffTree[root].nRight == -1)
	{
		char saved = a[i];
		a[i] = 0;
		strcpy(tBit[HuffTree[root].c], a);
		a[i] = saved;
		return;
	}
	a[i] = 48;
	MakeBit(a, i + 1, HuffTree[root].nLeft);
	a[i] = 49;
	MakeBit(a, i + 1, HuffTree[root].nRight);
	a[i] = 0;
}
bool Compression::Output(char *s, char *si, int id)
{
	FILE *f, *fi;
	f = fopen(s, "wb");
	fi = fopen(si, "rb");
	if (f == NULL || fi == NULL)
		return false;
	// ghi file
	unsigned char buffin = 0, buffout = 0;
	int i = 0;
	while (fread(&buffin, 1, 1, fi) > 0)
	{
		// new
		for (int j = 0; j < strlen(tBit[buffin]); j++)
		{
			if (i < 8)
			{
				buffout <<= 1;
				buffout |= tBit[buffin][j] - 48;
				i++;
			}
			if (i == 8)
			{
				i = 0;
				fwrite(&buffout, 1, 1, f);
			}
		}
	}

	if (i > 0)
	{
		buffout <<= (8 - i);
		fwrite(&buffout, 1, 1, f);
		buffout = 8 - i;
	}
	else
		buffout = 0;
	list[id].NullBit = buffout;
	fclose(f);
	fclose(fi);
	return true;
}


DeCompression::DeCompression()
{
	int i = 512;
	while (--i >= 0)
		HuffTree[i].nLeft = HuffTree[i].nRight = -1;
	f = NULL;
}
DeCompression::~DeCompression()
{
	delete[]list;
}
void DeCompression::MakeHuffTree()
{
	int inext = 1;
	int id;
	for (int i = 0; i < 256; i++)
	if (tBit[i][0] != 0)
	{
		id = 0;
		int n = strlen(tBit[i]), j;
		for (j = 0; j < n - 1; j++)
		{
			if (tBit[i][j] == '1')
			{
				if (HuffTree[id].nRight == -1)
				{
					HuffTree[id].nRight = inext;
					inext++;
				}
				id = HuffTree[id].nRight;
			}
			else
			{
				if (HuffTree[id].nLeft == -1)
				{
					HuffTree[id].nLeft = inext;
					inext++;
				}
				id = HuffTree[id].nLeft;
			}
		}
		// nut la:
		if (tBit[i][j] == '1')
			HuffTree[id].nRight = inext;
		else
			HuffTree[id].nLeft = inext;
		HuffTree[inext].c = i;
		inext++;
	}
}

void DeCompression::DeCompress(char *si)
{
	
	char st[256];
	strcpy(st, si);
	int i = strlen(st);
	while (st[i] != 92)
		st[i--] = 0;
	st[i] = 0;
	for (i = 0; i < filenum; i++)
	{
		Output(i, st);
	}
	fclose(f);
}
bool DeCompression::Input(char *s)
{
	f = fopen(s, "rb");
	char buff[13] = { 0 };
	fread(buff, 1, 12, f);
	if (strcmp(buff, "nhom9ctdl&gt") != 0)
		return false;

	fread(tBit, 1, 5120, f);
	fread(&filenum, sizeof(uint32_t), 1, f);
	list = new FileInfo[filenum];
	int x = ftell(f);
	fread(list, sizeof(FileInfo), filenum, f);
	return true;
}
bool DeCompression::Output(int i, char *si)
{
	FILE *fo;
	char st[256];
	sprintf(st, "%s\\%s", si, list[i].fname);
	fo = fopen(st, "wb");
	fseek(f, list[i].ps, SEEK_SET);
	long n, pcur;
	pcur = list[i].ps;
	n = list[i].pe;

	int root = 0, id = 0; // root: vi tri node dau tien trong cay, id: bit dang xu ly trong buff
	unsigned char buff; // bo dem
	while (pcur < n)
	{
		fread(&buff, 1, 1, f);
		pcur++;
		if (pcur == n)
			id = list[i].NullBit;
		while (id < 8)
		{
			if (HuffTree[root].nLeft == -1 && HuffTree[root].nRight == -1)
			{
				fwrite(&HuffTree[root].c, 1, 1, fo);
				root = 0;
			}
			else
			{
				unsigned char x;
				x = buff & 128;
				x >>= 7;
				buff <<= 1;
				if (x == 1)
					root = HuffTree[root].nRight;
				else
					root = HuffTree[root].nLeft;
				id++;
			}
		}
		if (HuffTree[root].nLeft == -1 && HuffTree[root].nRight == -1)
		{
			fwrite(&HuffTree[root].c, 1, 1, fo);
			root = 0;
		}
		id = 0;
	}
	// dong file
	fclose(fo);
	return true;
}